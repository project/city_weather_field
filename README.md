CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

City Weather field allows to return the weather of depends on the US 
city selected.

REQUIREMENTS
------------

No special requirements.

INSTALLATION
------------

Since the module requires an external library, Composer must be used.


CONFIGURATION
-------------

We need to setup the api key from http://openweathermap.org/ 
in /admin/config/city_weather_field/settings 
before using the module.


MAINTAINERS
-----------

Current maintainers:
 * Mohamed Anis Taktak (matio89) - https://www.drupal.org/u/matio89
 
 