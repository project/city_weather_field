<?php

namespace Drupal\city_weather_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\city_weather_field\WeatherService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'weather_widget' formatter.
 *
 * @FieldFormatter(
 *   id = "weather_formatter",
 *   label = @Translation("Weather"),
 *   field_types = {
 *     "weather_field_type"
 *   }
 * )
 */
class WeatherFieldFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * The weather service allow to return weather informations.
   *
   * @var \Drupal\city_weather_field\WeatherService
   */
  protected $weatherService;

  /**
   * The configuration factory allows to extract openweather key.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Creates an instance of the plugin.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container to pull out services used in the plugin.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   *
   * @return static
   *   Returns an instance of this plugin.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('city_weather_field.default'),
      $container->get('config.factory')
    );
  }

  /**
   * WeatherFieldFormatter constructor.
   *
   * @param $plugin_id
   * @param $plugin_definition
   * @param FieldDefinitionInterface $field_definition
   * @param array $settings
   * @param $label
   * @param $view_mode
   * @param array $third_party_settings
   * @param WeatherService $weatherService
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, WeatherService $weather_service, ConfigFactoryInterface $config_factory) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->weatherService = $weather_service;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $config = $this->configFactory->get('city_weather_field.weathersettings');
    // Get OpenWeather API key.
    $open_weather_api_key = $config->get('api_key_openweather');
    $cities = city_weather_field_get_cities();
    foreach ($items as $delta => $item) {
      if (isset($cities[$item->value])) {
        // US City name.
        $city_name = $cities[$item->value];
        $weather_informations = $this->weatherService->getWeatherInformation($open_weather_api_key, $city_name);
        if(is_array($weather_informations)){
          $elements[$delta] = [
            '#theme' => 'city_weather_field',
            '#name' => $weather_informations['name'],
            '#description' => $weather_informations['description'],
            '#max_temperature' => $weather_informations['max_temperature'],
            '#min_temperature' => $weather_informations['min_temperature'],
            '#humidity' => $weather_informations['humidity'],
            '#wind' => $weather_informations['wind'],
            '#icon' => $weather_informations['icon'],
          ];
        }

      }
    }
    return $elements;
  }

}

