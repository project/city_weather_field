<?php

namespace Drupal\city_weather_field\Plugin\Field\FieldType;


use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'weather_field_type' field type.
 *
 * @FieldType(
 *   id = "weather_field_type",
 *   label = @Translation("City - weather"),
 *   description = @Translation("Weather field type."),
 *   default_widget = "weather_widget",
 *   default_formatter = "weather_formatter"
 * )
 */
class WeatherFieldType extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('City'));
    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'type' => 'char',
          'length' => 2,
          'not null' => FALSE,
        ],
      ],
      'indexes' => [
        'value' => ['value'],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('value')->getValue();
    return $value === NULL || $value === '';
  }

}

