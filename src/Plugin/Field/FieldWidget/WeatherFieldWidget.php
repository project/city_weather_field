<?php

namespace Drupal\city_weather_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'weather_widget' widget.
 *
 * @FieldWidget(
 *   id = "weather_widget",
 *   label = @Translation("US Cities"),
 *   field_types = {
 *     "weather_field_type"
 *   }
 * )
 */
class WeatherFieldWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $cities = city_weather_field_get_cities();
    $element['value'] = $element + [
        '#type' => 'select',
        '#options' => $cities,
        '#empty_value' => '',
        '#default_value' => (isset($items[$delta]->value) && isset($cities[$items[$delta]->value])) ? $items[$delta]->value : NULL,
        '#description' => $this->t('Select a city'),
      ];
    return $element;
  }
}

