<?php

namespace Drupal\city_weather_field;

use Drupal\Component\Serialization\Json;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class WeatherService.
 */
class WeatherService {

  /**
   * Base uri of openweather api.
   * @var string
   */
  public $baseUri = 'http://api.openweathermap.org/';

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Constructs a new WeatherService object.
   */
  public function __construct(ClientInterface $http_client){
    $this->httpClient = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container){
    return new static(
      $container->get('http_client')
    );
  }

  /**
   * Return the data from the API.
   *
   * @param string $apiKey
   * @param string $cityId
   *
   * @return array|bool
   */
  public function getWeatherInformation($apiKey = "495eda627dd4930be7478b9509bf49cd", $cityId = "arizona"){
    try {
      $apiUrl = $this->baseUri . "/data/2.5/weather?q=" . $cityId . "&lang=en&units=metric&APPID=" . $apiKey;

      $response = $this->httpClient->request('GET', $apiUrl);

    } catch (GuzzleException $e) {
      watchdog_exception('city_weather_field', $e);
      return FALSE;
    }
    $content = $response->getBody()->getContents();
    $data = Json::decode($content);
    $output = [];
    $output['name'] = isset($data['name']) ? $data['name'] : "";
    $output['description'] = isset($data['weather'][0]['description']) ? ucwords($data['weather'][0]['description']) : "";
    $output['max_temperature'] = isset($data['main']['temp_max']) ? $data['main']['temp_max'] : "";
    $output['min_temperature'] = isset($data['main']['temp_min']) ? $data['main']['temp_min'] : "";
    $output['humidity'] = isset($data['main']['humidity']) ? $data['main']['humidity'] : "";
    $output['wind'] = isset($data['wind']['speed']) ? $data['wind']['speed'] : "";
    $output['icon'] = isset($data['weather'][0]['icon']) ? $data['weather'][0]['icon'] . '.png' : '';

    return $output;
  }

}
