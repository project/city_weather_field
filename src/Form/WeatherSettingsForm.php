<?php

namespace Drupal\city_weather_field\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class WeatherSettingsForm.
 */
class WeatherSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'city_weather_field.weathersettings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'weather_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('city_weather_field.weathersettings');
    $form['api_key_openweather'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API KEY - OpenWeather'),
      '#description' => $this->t('OpenWeatherMap API Key.'),
      '#required' => TRUE,
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('api_key_openweather'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('city_weather_field.weathersettings')
      ->set('api_key_openweather', $form_state->getValue('api_key_openweather'))
      ->save();
  }

}
